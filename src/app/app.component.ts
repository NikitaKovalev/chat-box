import { Component, OnInit } from '@angular/core';
import { IUsersList } from './interfaces/users';

import { USERS } from './mocks/users';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {

  public usersList: IUsersList[];

  constructor() {
    this.usersList = USERS;
  }

  public ngOnInit(): void {
  }
}
