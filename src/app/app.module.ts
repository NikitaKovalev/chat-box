// Angular
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';

// Material
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {
  MatButtonModule,
  MatCardModule,
  MatDialogModule,
  MatFormFieldModule,
  MatIconModule,
  MatInputModule,
  MatToolbarModule
} from '@angular/material';

// Components
import { AppComponent } from './app.component';
import { ChatBoxComponent } from './chat-box/chat-box.component';

// Material
import { MessageBoxComponent } from './message-box/message-box.component';
import { ReactiveFormsModule } from '@angular/forms';

// Services
import { SendMessageService } from './services/send-message.service';
import { DialogComponent } from './message-box/dialog/dialog.component';

@NgModule({
  declarations: [
    AppComponent,
    ChatBoxComponent,
    MessageBoxComponent,
    DialogComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatToolbarModule,
    MatCardModule,
    MatButtonModule,
    MatFormFieldModule,
    MatInputModule,
    MatIconModule,
    ReactiveFormsModule,
    MatDialogModule
  ],
  entryComponents: [
    DialogComponent
  ],
  providers: [
    SendMessageService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
