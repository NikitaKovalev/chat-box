export interface IMessage {
  id: number;
  name: string;
  lastName: string;
  avatar: string;
  message: string;
}
