export const USERS = [
  {
    id: 1,
    name: 'John',
    lastName: 'Rid',
    avatar: 'https://pp.userapi.com/c855332/v855332818/5a042/yqpGtNvsMiA.jpg',
  },
  {
    id: 2,
    name: 'Margaret',
    lastName: 'Rid',
    avatar: 'https://pp.userapi.com/c850016/v850016818/1a2a79/b6vOLC1xjLc.jpg',
  },
  {
    id: 4,
    name: 'Garry',
    lastName: 'May',
    avatar: 'https://pp.userapi.com/c855632/v855632818/5d560/WSeCbYZ7Zsw.jpg',
  }
];
