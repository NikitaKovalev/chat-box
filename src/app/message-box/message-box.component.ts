// Angular
import { Component, OnDestroy, OnInit } from '@angular/core';

// Services
import { SendMessageService } from '../services/send-message.service';

// Interface
import { IMessage } from '../interfaces/message';

// Rxjs
import { Subject } from 'rxjs';
import { debounceTime, takeUntil } from 'rxjs/operators';
import { MatDialog } from '@angular/material';
import { DialogComponent } from './dialog/dialog.component';

@Component({
  selector: 'message-box',
  templateUrl: './message-box.component.html',
  styleUrls: ['./message-box.component.scss']
})
export class MessageBoxComponent implements OnInit, OnDestroy {

  public loaded = false;
  public messagesList: IMessage[] = [];
  private _destroyed$ = new Subject<any>();

  constructor(private _sendMessageService: SendMessageService,
              private _dialog: MatDialog) {
  }

  public ngOnInit(): void {
    this._subscribeToMessage();
  }

  public ngOnDestroy(): void {
    this._destroyed$.complete();
    this._destroyed$.next();
  }

  public removeMessage(message): void {
    const index = this.messagesList.indexOf(message);
    const dialogRef = this._dialog.open(DialogComponent, {
      width: '400px',
    });

    dialogRef.afterClosed()
      .subscribe(result => {
        if ( result ) {
          if ( index !== -1 ) {
            setTimeout(_ => {
              this.messagesList.splice(index, 1);
            }, 300);
          }
        }
      });
  }

  private _subscribeToMessage(): void {
    this._sendMessageService.getMessages()
      .pipe(
        takeUntil(this._destroyed$),
        debounceTime(300)
      )
      .subscribe((message: IMessage) => {
        this.loaded = true;
        this.messagesList.push(message);
      });
  }
}
