import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

import { IMessage } from '../interfaces/message';

@Injectable()
export class SendMessageService {

  public messageSubscriber = new Subject<IMessage>();

  constructor() { }

  public setMessage(message: IMessage): void {
    this.messageSubscriber.next(message);
  }

  public getMessages() {
    return this.messageSubscriber;
  }

}
