// Angular
import { Component, ElementRef, Input, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';

// Interfaces
import { IUsersList } from '../interfaces/users';

// Services
import { SendMessageService } from '../services/send-message.service';


@Component({
  selector: 'chat-box',
  templateUrl: './chat-box.component.html',
  styleUrls: ['./chat-box.component.scss']
})
export class ChatBoxComponent implements OnInit {

  @Input() user: IUsersList;
  public messageForm: FormGroup;

  constructor(private _fb: FormBuilder,
              private _sendMessageService: SendMessageService) { }

  ngOnInit() {
    this._formInit();
  }

  public onSubmit(): void {
    const bodyParams = this.messageForm.getRawValue();

    if (bodyParams.message) {
      this._sendMessageService.setMessage(bodyParams);
    }
  }

  private _formInit(): void {
    this.messageForm = this._fb.group({
      id: new FormControl(this.user.id),
      name: new FormControl(this.user.name),
      lastName: new FormControl(this.user.lastName),
      avatar: new FormControl(this.user.avatar),
      message: new FormControl(null, [Validators.required])
    });
  }

}
