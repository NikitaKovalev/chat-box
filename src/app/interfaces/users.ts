export interface IUsersList {
  id: number;
  name: string;
  lastName: string;
  avatar: string;
}
